@extends('master.master')

@section('style')
<link rel="stylesheet" href="{{ asset('css/dashboard.css') }}" />
@endsection

@section('content')

<section>
    <div class="row mb-3 ">
        <div class="col-12 d-flex align-items-center">
            <h3 class="text-light">Home</h3>
            <div class="dropdown ms-auto">
                <button class="btn btn-outline-light dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                    New
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item" href="#">Action</a></li>
                    <li><a class="dropdown-item" href="#">Another action</a></li>
                    <li><a class="dropdown-item" href="#">Something else here</a></li>
                </ul>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-4">
            <div class="card shadow-sm">
                <div class="card-header" style="font-size: 18px;">
                    <span class="rounded-circle-logo-span">CB</span>
                    Company B Admin
                    <span style="display:block; font-size: 12px; margin-left: 20px">admin</span>
                </div>
                <div class="row">
                    <div class="col-6 card-body text-center">
                        <h5>Vacation</h5>
                        <span style="color: rgb(19, 94, 124); font-size: 25px">
                            <i class="bi bi-tree-fill"></i> 8.3
                        </span>
                        <br>
                        <span style="color: rgb(19, 94, 124); font-size: 15px; font-weight: 600">HOURS AVAILABLE</span>
                        <p>48 hours scheduled</p>
                    </div>
                    <div class="col-6 card-body text-center">
                        <h5>Sick</h5>
                        <span style="color: rgb(19, 94, 124); font-size: 25px">
                            <i class="bi bi-x-diamond-fill"></i></i> - 136
                        </span>
                        <br>
                        <span style="color: rgb(19, 94, 124); font-size: 15px; font-weight: 600">HOURS AVAILABLE</span>
                    </div>

                    <div class="col-9 card-body text-center">
                        <button style="background-color: rgb(19, 94, 124); color: white; font-size: 15px; font-weight: 200; width: 95%"><i class="bi bi-send-check-fill"></i> REQUEST TIME OFF</button>
                    </div>
                    <div class="col-3 card-body text-center">
                        <span style="font-size: 20px"><i class="bi bi-calculator-fill"></i></span>
                    </div>
                </div>
               
            </div>
        </div>
        <div class="col-8">
            <div class="card shadow-sm">
                <div class="card-header d-flex align-items-center">
                    What's happning in company B
                    <span class="ms-auto" style="font-size: 12px;">18 Notificaton</span>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Special title treatment</h5>
                    <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                    <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
                <div class="card-footer text-muted">
                    2 days ago
                </div>
            </div>
        </div>
    </div>
</section>

@endsection