<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard</title>

    {{-- bootstrap --}}
    <link rel="stylesheet" href="{{ asset('bootstrap-5.0.2-dist/css/bootstrap.min.css') }}">

    {{-- css --}}
    <link rel="stylesheet" href="{{asset('css/master.css')}}">

    {{-- icon --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.0/font/bootstrap-icons.css">

    @yield('style')
</head>

<body>
    <!-- Headder Navigation bar -->
    <header>
        <nav class="navbar navbar-expand-md navbar-light fixed-top bg-grad bg-light">
            <div class="container-fluid">
                <a class="navbar-brand" href="/dashboard">
                    <span class="rounded-circle-logo">CB</span>
                    <span> COMPANY B </span>
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse ms-2" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="/dashboard"> Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/my-info">My info</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/people">People</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/hiring">Hiring</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/reports">Reports</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/files">Files</a>
                        </li>
                    </ul>
                    <form class="d-flex">
                        <input class="form-control me-2 search" type="search" placeholder="Search" aria-label="Search">
                    </form>
                    <ul class="navbar-nav ms-2 mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="/nor"> <i class="bi bi-bell-fill"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/quv"><i class="bi bi-question-circle-fill"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/setting"><i class="bi bi-gear-fill"></i></a>
                        </li>
                        <li class="nav-item">
                            <span class="rounded-circle-logo">CB</span>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="section-colour">

        </div>
    </header>
    <!-- Headder Navigation bar -->

    <!-- Body content Here -->
    <main class="container page-body">
        @yield('content')
    </main>
    <!-- #Body content -->


    {{-- footer start --}}
    <footer class="bg-light text-lg-start  b-0 row" >
        <!-- Copyright -->
        <div class="col-1" style="background-color: rgba(0, 0, 0, 0.2);"></div>
        <div class="p-3 col-5" style="background-color: rgba(0, 0, 0, 0.2);">
           Privacy Policy . terms of Service © 2008 - 2021 zWorks All right reserved
        </div>
        <div class="col-4" style="background-color: rgba(0, 0, 0, 0.2);"></div>
        <div class="p-3 col-2" style="background-color: rgba(0, 0, 0, 0.2);">
            <a class="text-dark" target="_black" href="https://zworkspte.com/">zWorks pte</a>
        </div>
        <!-- Copyright -->
    </footer>

</body>

<script src="{{ asset('bootstrap-5.0.2-dist/js/bootstrap.bundle.min.js') }}"></script>

</html>